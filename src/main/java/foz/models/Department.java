package foz.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;
@Entity
@Table(name="department")
public class Department {

    @NotNull
    private String name;

    @NotNull
    private String password;



    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "department_id")
    private long departmentId;

    @OneToMany(mappedBy = "department")
    @JsonManagedReference(value = "department-st")
    private Set<Student> students;

    @ManyToMany
    @JsonIgnore
    @JoinTable(name = "roles",
            joinColumns = {@JoinColumn(name = "department_id")},
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;

    public Department(){}

    public Department(long departmentId){this.departmentId = departmentId;}

    public  Department(String name, String password){
        this.password = password;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(long departmentId) {
        this.departmentId = departmentId;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public void setStudents(Set<Student> students) {
            this.students = students;
        }
}
