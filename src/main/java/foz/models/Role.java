package foz.models;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.Set;

@Entity
    @Table(name="role")
public class Role implements GrantedAuthority {
    @Id
    @Column(name = "role_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long roleId;
    private String role;

    @ManyToMany(mappedBy = "roles")
    private Set<SuperUser> users;

    /*@ManyToMany(mappedBy = "roles")
    private Set<Student> students;

    @ManyToMany(mappedBy = "roles")
    private Set<Employer> employers;
*/
    @ManyToMany(mappedBy = "roles")
    private Set<Department> departments;

    /*public Set<Student> getStudents() {
        return students;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }
*/
    public long getId() {
        return roleId;
    }

    public void setId(long id) {
        this.roleId = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    protected Role(){}
    public Role(String name)
    {
        role = name;
    }

    @Override
    public String getAuthority() {
        return getRole();
    }

    /*public Set<Employer> getEmployers() {
        return employers;
    }

    public void setEmployers(Set<Employer> employers) {
        this.employers = employers;
    }*/

    public Set<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(Set<Department> departments) {
        this.departments = departments;
    }

    public Set<SuperUser> getUsers() {
        return users;
    }

    public void setUsers(Set<SuperUser> users) {
        this.users = users;
    }
}




