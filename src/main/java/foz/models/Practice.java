package foz.models;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "practices")
public class Practice {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "practice_id")
    private long pacticeId;


    @NotNull
    private Date startDate;

    @NotNull
    private String name;

    @NotNull
    private Date endDate;

    @NotNull
    private String description;

    @OneToMany(mappedBy = "practice")
    @JsonManagedReference
    private Set<Student> students;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "employer_id")
    @JsonBackReference(value = "employer-pr")
    private Employer employer;

    public Practice() {
    }

    public Practice(String name, Date startDate, Date endDate, String description){
        this.description = description;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Employer getEmployer() {
        return employer;
    }

    public void setEmployer(Employer employer) {
        this.employer = employer;
    }
}