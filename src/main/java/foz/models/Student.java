package foz.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name = "students")
@DiscriminatorValue("stu")
public class Student extends SuperUser {

    /*@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "student_id")
    private long studentId;*/

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "department_id")
    @JsonBackReference(value = "department-st")
    private Department department;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "practice_id")
    @JsonBackReference
    private Practice practice;

    /*@ManyToMany
    @JoinTable(name = "roles",
            joinColumns = {@JoinColumn(name = "student_id")},
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;*/

    /*public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }*/


    public Student() { }


    public Student(String email, String name, String password) {
        this.password = password;
        this.email = email;
        this.username = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String value) {
        this.email = value;
    }

    public String getName() {
        return username;
    }

    public void setName(String value) {
        this.username = value;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Practice getPractice() {
        return practice;
    }

    public void setPractice(Practice practice) {
        this.practice = practice;
    }


   /* public long getStudentId() {
        return studentId;
    }

    public void setStudentId(long studentId) {
        this.studentId = studentId;
    }*/
}