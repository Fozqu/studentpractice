package foz.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name = "employers")
@DiscriminatorValue("emp")
public class Employer extends SuperUser{

   /* @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "employer_id")*/
    //private long employerId;

    /*@NotNull
    private String name;

    @NotNull
    private String email;

    @NotNull
    private String password;*/

    @OneToMany(mappedBy = "employer")
    @JsonManagedReference(value = "employer-pr")
    private Set<Practice> practices;

    /*@ManyToMany
    @JsonIgnore
    @JoinTable(name = "roles",
            joinColumns = {@JoinColumn(name = "employer_id")},
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;*/

    public Employer(){}

    public Employer(String email, String username, String password){
        this.email = email;
        this.username = username;
        this.password = password;

    }

    public Set<Practice> getPractices() {
        return practices;
    }


    public void setPractices(Set<Practice> practices) {
        this.practices = practices;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return username;
    }

    public void setName(String username) {
        this.username = username;
    }


   /* public long getEmployerId() {
        return employerId;
    }

    public void setEmployerId(long employerId) {
        this.employerId = employerId;
    }*/
}
