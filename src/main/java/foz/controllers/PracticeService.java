package foz.controllers;


import foz.dao.EmployerDao;
import foz.dao.PracticeDao;
import foz.dao.StudentDao;
import foz.models.Employer;
import foz.models.Practice;
import foz.models.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@Controller
public class PracticeService {
    @Autowired
    private PracticeDao practiceDao;
    @Autowired
    private StudentDao studentDao;
    @Autowired
    private EmployerDao employerDao;

    @RequestMapping(value = "/practices/create", method = RequestMethod.POST)
    @ResponseBody
    public String createPractice(@RequestBody Practice practice){
        Practice pr = null;
        pr = new Practice(practice.getName(), practice.getStartDate(), practice.getEndDate(), practice.getDescription());
        System.out.print(practice.getEmployer());
        practiceDao.save(pr);
        return "Practice created";
    }

    @RequestMapping(value = "/practices", method = RequestMethod.GET)
    public @ResponseBody
            List<Practice> getAllPractices(){
        return (List<Practice>) practiceDao.findAll();
    }


    @RequestMapping(value = "/practices/{practice_id}/apply", method = RequestMethod.POST)
    public @ResponseBody String apply(@RequestParam String student_id, @PathVariable String practice_id){
        Practice pr = null;
        Student st = null;
        st = studentDao.findOne(Long.parseLong(student_id));
        pr = practiceDao.findOne(Long.parseLong(practice_id));
        st.setPractice(pr);
        studentDao.save(st);
        return "Sucsessfully";
    }

}
