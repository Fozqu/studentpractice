package foz.controllers;


        import foz.dao.SuperUserDao;
        import foz.models.Department;
        import foz.dao.DepartmentDao;
        import foz.models.Student;
        import foz.dao.StudentDao;
        import foz.models.SuperUser;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Controller;
        import org.springframework.web.bind.annotation.*;
        import java.util.List;


@Controller
public class StudentController {
    @Autowired
    private DepartmentDao departmentDao;
    @Autowired
    private StudentDao studentDao;
    @Autowired
    private SuperUserDao superUserDao;


    //Create new Student
    @RequestMapping(value = "student/create",method = RequestMethod.POST)
    @ResponseBody
    public String create(@RequestBody Student st) {
        Student student = null;
        try {
            student = new Student(st.getEmail(), st.getName(), st.getPassword());
            studentDao.save(student);
        }
        catch (Exception ex) {
            return "Error creating the student: " + ex.toString();
        }
        return "Student succesfully created! (id = " + student.getId() + ")";
    }


    @RequestMapping("/delete")
    @ResponseBody
    public String delete(long id) {
        try {
            studentDao.delete(id);
        }
        catch (Exception ex) {
            return "Error deleting the user:" + ex.toString();
        }
        return "Student succesfully deleted!";
    }

    @RequestMapping(value = "/student", method = RequestMethod.GET)
    @ResponseBody
    public List<Student> getAllStudents(){

        return (List<Student>) studentDao.findAll();
    }

    /*@RequestMapping("/get-by-email-department")
    @ResponseBody
    public String getByEmail(String email) {
        String userId;
        try {
            Student student = studentDao.findByEmail(email);
            userId = String.valueOf(student.getStudentId());
        }
        catch (Exception ex) {
            return "Student not found";
        }
        return "The user id is: " + userId;
    }*/

    @RequestMapping("/department/{departmentName}/{studentId}/apply")
    @ResponseBody
    public String apply(@PathVariable String studentId, @PathVariable String departmentName){
        Student st;
        Department dp;
        st = studentDao.findOne(Long.parseLong(studentId));
        dp = departmentDao.findByName(departmentName);
        System.out.println(dp.getName());
        st.setDepartment(dp);
        studentDao.save(st);
        System.out.println(st.getDepartment().getName());
        return "Applyed";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public String updateUser(long id, String email, String name) {
        try {
            Student student = studentDao.findOne(id);
            student.setEmail(email);
            student.setName(name);
            studentDao.save(student);
        }
        catch (Exception ex) {
            return "Error updating the user: " + ex.toString();
        }
        return "Student succesfully updated!";
    }



    @RequestMapping(value = "student/test", method = RequestMethod.GET)
    public @ResponseBody
    Iterable<Student> test() {

   return studentDao.findAll();
    }
}