package foz.controllers;

import foz.dao.EmployerDao;
import foz.models.Employer;
import foz.models.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class EmpController {
    @Autowired
    private EmployerDao employerDao;

    @RequestMapping(value = "employer/create",method = RequestMethod.POST)
    @ResponseBody
    public String create(@RequestBody Employer st) {
        Employer employer = null;
        try {
            employer = new Employer(st.getEmail(), st.getName(), st.getPassword());
            employerDao.save(employer);
        }
        catch (Exception ex) {
            return "Error creating the student: " + ex.toString();
        }
        return "Student succesfully created! (id = " + employer.getId() + ")";
    }
}
