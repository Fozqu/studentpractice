package foz.dao;


import foz.models.Practice;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PracticeDao extends CrudRepository<Practice, Long>{

}
