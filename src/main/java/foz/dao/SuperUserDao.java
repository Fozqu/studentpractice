package foz.dao;

import foz.models.Student;
import foz.models.SuperUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SuperUserDao extends CrudRepository<SuperUser,Long> {
    SuperUser findByUsername(String name);
    SuperUser findByEmail(String email);
}
