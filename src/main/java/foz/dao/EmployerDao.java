package foz.dao;

import foz.models.Employer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployerDao extends CrudRepository<Employer,Long> {

}
