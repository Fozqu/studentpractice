package foz.dao;

import javax.transaction.Transactional;

import foz.models.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Transactional
@Repository
public interface StudentDao extends CrudRepository<Student, Long> {


     Student findByEmail(String email);
     Student findByUsername(String username);

}