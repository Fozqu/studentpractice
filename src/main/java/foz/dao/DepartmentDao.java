package foz.dao;

import foz.models.Department;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface DepartmentDao extends CrudRepository<Department, Long> {
     Department findByName(String departmentName);
}
